﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace IniHelper
{
    public class Program
    {
        //HKEY_CURRENT_USER\Software\Classes\dllfile\shell\Create ini file\command
        //"c:\Users\spl\Documents\Visual Studio 2013\Projects\IniHelper\IniHelper\bin\Release\IniHelper.exe" "%1"
        //HKEY_CURRENT_USER\Software\Classes\dllfile\shell (Default) = Browse with .NET Reflector
        public static void Main(string[] args)
        {
            string file = args[0];
            int index = file.LastIndexOf('.');
            var iniFilePath = file.Substring(0, index + 1) + "ini";

            using (var writer = new StreamWriter(iniFilePath))
            {
            writer.Write(@"[.NET Framework Debugging Control]
GenerateTrackingInfo=1
AllowOptimize=0");
            }
           
        }
    }
}
