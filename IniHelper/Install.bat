@ECHO off
IF NOT EXIST "%CD%\IniHelper.exe" ECHO File "%CD%\IniHelper.exe" should exist. Cannot complete operation.
IF NOT EXIST "%CD%\IniHelper.exe" PAUSE
IF NOT EXIST "%CD%\IniHelper.exe" exit

REG ADD "hkcu\Software\Classes\dllfile\shell\Create ini file" /ve /t REG_SZ /d "Create ini file" /f
REG ADD "hkcu\Software\Classes\dllfile\shell\Create ini file\command" /ve /t REG_SZ /d "%CD%\IniHelper.exe %%1" /f

ECHO Context menu handlers were added successfully.
PAUSE